# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=polkit-kde-agent-1
pkgver=5.20.2
pkgrel=0
pkgdesc="Daemon providing a polkit authentication UI for KDE"
# armhf blocked by extra-cmake-modules
# mips, mips64, s390x blocked by polkit-qt-1
arch="all !armhf !s390x !mips !mips64"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later"
depends="polkit-elogind"
makedepends="extra-cmake-modules qt5-qtbase-dev ki18n-dev kwindowsystem-dev kdbusaddons-dev kwidgetsaddons-dev kcoreaddons-dev kcrash-dev kiconthemes-dev polkit-qt-1-dev"
source="https://download.kde.org/stable/plasma/$pkgver/polkit-kde-agent-1-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="d65168af032513752225302803b7f169256178cca9e4bb5a89b57499177833972a8177f23a00a621b3ccc42f3ad99f435ee8564f26899ddceb3f9257c0a9a0c7  polkit-kde-agent-1-5.20.2.tar.xz"
